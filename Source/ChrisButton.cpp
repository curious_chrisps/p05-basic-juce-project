//
//  ChrisButton.cpp
//  JuceBasicWindow - App
//
//  Created by Christoph Schick on 24/10/2017.
//

#include "ChrisButton.hpp"


ChrisButton::ChrisButton()
{
}

ChrisButton::~ChrisButton()
{
}

void ChrisButton::paint(Graphics& g)
{
    g.setColour(Colours::white);
    g.drawEllipse(5, 5, getWidth()-10, getHeight()-10, 3.0);
    
    if (mouseIsOver)
    {
        g.setColour(Colours::palevioletred);
        
        if (mouseIsDown)
        {
            g.setColour(Colours::grey);
        }
    }else
    {
        g.setColour(Colours::blue);
    }
   
    g.fillEllipse(5, 5, getWidth()-10, getHeight()-10);
}

void ChrisButton::mouseEnter(const MouseEvent&)
{
    mouseIsOver = true;
    repaint();
}

void ChrisButton::mouseExit  (const MouseEvent&)
{
    mouseIsOver = false;
    repaint();
}
void ChrisButton::mouseDown  (const MouseEvent&)
{
    mouseIsDown = true;
    repaint();
}
void ChrisButton::mouseUp    (const MouseEvent&)
{
    mouseIsDown = false;
    repaint();
}
