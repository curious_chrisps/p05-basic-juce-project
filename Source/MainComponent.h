/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "ChrisButton.hpp"
#include "ButtonRow.hpp"


//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainContentComponent   :  public Component
{
public:
    //==============================================================================
    MainContentComponent();
    ~MainContentComponent();

    void resized() override;
    void paint(Graphics& g) override;
    void mouseUp(const MouseEvent& event) override;

private:
    //==============================================================================
    int mousePosX = 0, mousePosY = 0;
    
//    ChrisButton myOwnButton;
    ButtonRow thisIsTheButtonRow;
 
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainContentComponent)
};
