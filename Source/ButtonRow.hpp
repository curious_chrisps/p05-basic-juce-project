//
//  ButtonRow.hpp
//  JuceBasicWindow - App
//
//  Created by Christoph Schick on 28/10/2017.
//

#pragma once
#include "../JuceLibraryCode/JuceHeader.h"
#include "ChrisButton.hpp"
#include <stdio.h>
#define NUM_OF_BUTTONS_IN_ROW 5

class ButtonRow : public Component
{
public:
    ButtonRow();
    ~ButtonRow();
    
    void resized() override;
    
private:
    ChrisButton rowButtons[NUM_OF_BUTTONS_IN_ROW];
};
