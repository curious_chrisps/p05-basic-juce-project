//
//  ButtonRow.cpp
//  JuceBasicWindow - App
//
//  Created by Christoph Schick on 28/10/2017.
//

#include "ButtonRow.hpp"

ButtonRow::ButtonRow()
{
    for (int i = 0; i < NUM_OF_BUTTONS_IN_ROW; i++) {
        addAndMakeVisible(rowButtons[i]);
    }
}

ButtonRow::~ButtonRow()
{
}

void ButtonRow::resized()
{
    for (int i = 0; i < NUM_OF_BUTTONS_IN_ROW ; i++) {
        rowButtons[i].setBounds(10+(i * (getWidth()/NUM_OF_BUTTONS_IN_ROW)), 10, (getWidth()/NUM_OF_BUTTONS_IN_ROW) - 10, getHeight() - 10);
    }
}
