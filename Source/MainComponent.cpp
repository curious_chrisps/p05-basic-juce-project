/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainContentComponent::MainContentComponent()
{
    setSize (600, 400);
    
    addAndMakeVisible(thisIsTheButtonRow);
}

MainContentComponent::~MainContentComponent()
{
}


void MainContentComponent::resized()
{
    // This is called when the MainContentComponent is resized.
    // If you add any child components, this is where you should
    // update their positions.
   
    // Prints the current width and height of the main window when it's being resized.
    // Un-comment to activate.
//    DBG("Width: " << getWidth() << "    Height: " << getHeight());
    
//    colourSelector1.setBounds(0, 0, getWidth(), getHeight()/2);
    
    thisIsTheButtonRow.setBounds(10, getHeight()/3, getWidth() - 50, getHeight()/4);

}


void MainContentComponent::paint(Graphics& g)
{
    //Lets you see that this function is called
//    DBG("PAINTING...\n");
//    Colour ellipseCol = colourSelector1.getCurrentColour();
//    g.setColour(ellipseCol);
//    g.fillEllipse(mousePosX-25, mousePosY-25, 50, 50);

}

void MainContentComponent::mouseUp(const MouseEvent& event)
{
//    DBG("Mouse released at: x/y = " << event.x << " / " << event.y);
//    mousePosX = event.x;
//    mousePosY = event.y;
//    repaint();
}
