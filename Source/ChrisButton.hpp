//
//  ChrisButton.hpp
//  JuceBasicWindow - App
//
//  Created by Christoph Schick on 24/10/2017.
//

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

class ChrisButton   :   public Component
{
public:
    ChrisButton();
    ~ChrisButton();
    
    
    void paint      (Graphics& g)       override;
    
    void mouseEnter (const MouseEvent&) override;
    void mouseExit  (const MouseEvent&) override;
    void mouseDown  (const MouseEvent&) override;
    void mouseUp    (const MouseEvent&) override;
    
private:
    bool mouseIsOver = false;
    bool mouseIsDown = false;
};
